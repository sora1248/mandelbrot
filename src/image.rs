use crate::utils::ArgumentError;
use std::error::Error;
use std::fs::File;
use std::io::Write;

pub fn save_pgm(
    filename: &str,
    width: usize,
    height: usize,
    data: &Vec<u8>,
) -> Result<(), Box<dyn Error>> {
    if data.len() != width * height {
        return Err(Box::new(ArgumentError::new(
            "data",
            "Array length doesn't match image size!",
        )));
    }
    let mut file = File::create(filename)?;
    write!(file, "P5\n{} {}\n255\n", width, height)?;
    file.write(data)?;
    Ok(())
}
