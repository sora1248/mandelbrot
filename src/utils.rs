use std::error::Error;
use std::fmt::{Debug, Display, Formatter};

pub struct ArgumentError {
    argument: &'static str,
    description: String,
}

impl ArgumentError {
    pub fn new(argument: &'static str, description: &str) -> ArgumentError {
        ArgumentError {
            argument,
            description: String::from(description),
        }
    }
}

impl Error for ArgumentError {}

impl Display for ArgumentError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Argument \"{}\" wrong: {}",
            self.argument, self.description
        )
    }
}

impl Debug for ArgumentError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        Display::fmt(&self, f)
    }
}
