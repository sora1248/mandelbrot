use mandelbrot::image::save_pgm;
use mandelbrot::mandelbrot::Mandelbrot;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let mandelbrot = Mandelbrot::new(1024);

    save_pgm(
        "test.pgm",
        mandelbrot.width,
        mandelbrot.height,
        &mandelbrot.data,
    )?;

    Ok(())
}
