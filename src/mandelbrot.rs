use num_complex::Complex64;

pub struct Mandelbrot {
    pub width: usize,
    pub height: usize,
    pub res: usize,
    pub data: Vec<u8>,
}

impl Mandelbrot {
    pub fn new(res: usize) -> Mandelbrot {
        let width = 3 * res;
        let height = 5 * res / 2;
        let mut mandelbrot = Mandelbrot {
            width,
            height,
            res,
            data: vec![0; width * height],
        };
        mandelbrot.calc();
        mandelbrot
    }

    fn calc(&mut self) {
        let max_iter = 100;
        for y in 0..self.height {
            let sl = &mut self.data[y * self.width..(y + 1) * self.width];
            let cy = y as f64 / self.res as f64 - 1.25;
            for x in 0..self.width {
                let cx = x as f64 / self.res as f64 - 2.25;
                let c = Complex64::new(cx, cy);
                let mut z = Complex64::default();
                let mut iter = 0;
                while z.norm_sqr() < 1E6 && iter < max_iter {
                    z = z * z + c;
                    iter += 1
                }
                sl[x] = if iter == max_iter { 0 } else { 32 + iter % 224 };
            }
        }
    }
}
